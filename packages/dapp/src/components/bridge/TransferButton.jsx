import {
  Flex,
  Image,
  Link,
  Spinner,
  Text,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import TransferIcon from 'assets/transfer.svg';
import UnlockIcon from 'assets/unlock.svg';
import { TxLink } from 'components/common/TxLink';
import { ConfirmTransferModal } from 'components/modals/ConfirmTransferModal';
import { isRebasingToken } from 'components/warnings/RebasingTokenWarning';
import { isSafeMoonToken } from 'components/warnings/SafeMoonTokenWarning';
import { useBridgeContext } from 'contexts/BridgeContext';
import { useWeb3Context } from 'contexts/Web3Context';
import { utils } from 'ethers';
import { useBridgeDirection } from 'hooks/useBridgeDirection';
import { isRevertedError } from 'lib/amb';
import { ADDRESS_ZERO } from 'lib/constants';
import { formatValue, handleWalletError } from 'lib/helpers';
import React, { useCallback } from 'react';

export const TransferButton = () => {
  const { providerChainId } = useWeb3Context();
  const { foreignChainId, enableReversedBridge } = useBridgeDirection();
  const { isGnosisSafe, ethersProvider } = useWeb3Context();
  const {
    receiver,
    fromAmount: amount,
    fromToken: token,
    toToken,
    fromBalance: balance,
    tokenLimits,
    allowed,
    toAmountLoading,
    needsClaiming,
    approve,
    unlockLoading,
    approvalTxHash,
  } = useBridgeContext();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();
  const showError = useCallback(
    msg => {
      if (msg) {
        toast({
          title: 'Error',
          description: msg,
          status: 'error',
          isClosable: 'true',
        });
      }
    },
    [toast],
  );
  const isTokenRebasing = isRebasingToken(token);
  const isTokenSafeMoon = isSafeMoonToken(token);
  const showReverseBridgeWarning =
    !!toToken &&
    !enableReversedBridge &&
    toToken.chainId === foreignChainId &&
    toToken.address === ADDRESS_ZERO;
  const buttonEnabled =
    allowed &&
    !toAmountLoading &&
    !showReverseBridgeWarning &&
    !isTokenRebasing &&
    !isTokenSafeMoon;

  const valid = useCallback(() => {
    if (!ethersProvider) {
      showError('Please connect wallet');
    } else if (tokenLimits && amount.lt(tokenLimits.minPerTx)) {
      showError(
        `Please specify amount more than ${formatValue(
          tokenLimits.minPerTx,
          token.decimals,
        )}`,
      );
    } else if (tokenLimits && amount.gt(tokenLimits.maxPerTx)) {
      showError(
        `Please specify amount less than ${formatValue(
          tokenLimits.maxPerTx,
          token.decimals,
        )}`,
      );
    } else if (balance.lt(amount)) {
      showError('Not enough balance');
    } else if (receiver ? !utils.isAddress(receiver) : isGnosisSafe) {
      showError(`Please specify a valid recipient address`);
    } else {
      return true;
    }
    return false;
  }, [
    ethersProvider,
    tokenLimits,
    token,
    amount,
    balance,
    receiver,
    isGnosisSafe,
    showError,
  ]);

  const onClick = () => {
    if (buttonEnabled && valid()) {
      onOpen();
    }
  };

  const buttonDisabled =
    allowed || toAmountLoading || isTokenRebasing || isTokenSafeMoon;

  const validApprove = useCallback(() => {
    if (amount.lte(0)) {
      showError('Please specify amount');
      return false;
    }
    if (balance.lt(amount)) {
      showError('Not enough balance');
      return false;
    }
    return true;
  }, [amount, balance, showError]);

  const onClickApprove = useCallback(() => {
    if (!unlockLoading && !buttonDisabled && validApprove()) {
      approve().catch(error => {
        if (error && error.message) {
          if (
            isRevertedError(error) ||
            (error.data &&
              (error.data.includes('Bad instruction fe') ||
                error.data.includes('Reverted')))
          ) {
            showError(
              <Text>
                There is problem with the token unlock. Try to revoke previous
                approval if any on{' '}
                <Link
                  href="https://revoke.cash"
                  textDecor="underline"
                  isExternal
                >
                  https://revoke.cash/
                </Link>{' '}
                and try again.
              </Text>,
            );
          } else {
            handleWalletError(error, showError);
          }
        } else {
          showError(
            'Impossible to perform the operation. Reload the application and try again.',
          );
        }
      });
    }
  }, [unlockLoading, buttonDisabled, validApprove, showError, approve]);

  return (
    <div>
      {allowed ? (
        <Flex
          as="button"
          align="center"
          color={needsClaiming ? 'purple.300' : 'blue.500'}
          _hover={
            !buttonEnabled
              ? undefined
              : {
                  color: needsClaiming ? 'purple.400' : 'blue.600',
                }
          }
          cursor={!buttonEnabled ? 'not-allowed' : 'pointer'}
          transition="0.25s"
          position="relative"
          opacity={!buttonEnabled ? 0.4 : 1}
          onClick={onClick}
          borderRadius="0.25rem"
          w={{ base: '10rem', sm: '12rem', lg: 'auto' }}
        >
          <ConfirmTransferModal isOpen={isOpen} onClose={onClose} />
          <svg width="100%" viewBox="0 0 156 42" fill="none">
            <path
              d="M139.086 39.72a4 4 0 01-3.612 2.28H20.526a4 4 0 01-3.612-2.28l-16.19-34C-.54 3.065 1.395 0 4.335 0h147.33c2.94 0 4.875 3.065 3.611 5.72l-16.19 34z"
              fill="currentColor"
            />
          </svg>
          <Flex
            position="absolute"
            w="100%"
            h="100%"
            justify="center"
            align="center"
          >
            <Text color="white" fontWeight="bold">
              {needsClaiming ? 'Request' : 'Transfer'}
            </Text>
            <Image src={TransferIcon} ml={2} />
          </Flex>
        </Flex>
      ) : (
        <Flex
          align="center"
          as="button"
          color="cyan.500"
          _hover={
            buttonDisabled
              ? undefined
              : {
                  color: 'cyan.600',
                }
          }
          cursor={buttonDisabled ? 'not-allowed' : 'pointer'}
          transition="0.25s"
          position="relative"
          opacity={buttonDisabled ? 0.4 : 1}
          onClick={onClickApprove}
          borderRadius="0.25rem"
          w={{ base: '10rem', sm: '12rem', lg: 'auto' }}
        >
          <svg width="100%" viewBox="0 0 156 42" fill="none">
            <path
              d="M139.086 39.72a4 4 0 01-3.612 2.28H20.526a4 4 0 01-3.612-2.28l-16.19-34C-.54 3.065 1.395 0 4.335 0h147.33c2.94 0 4.875 3.065 3.611 5.72l-16.19 34z"
              fill="currentColor"
            />
          </svg>
          <Flex
            position="absolute"
            w="100%"
            h="100%"
            justify="center"
            align="center"
          >
            {unlockLoading ? (
              <TxLink chainId={providerChainId} hash={approvalTxHash}>
                <Spinner color="white" size="sm" />
              </TxLink>
            ) : (
              <>
                <Text color="white" fontWeight="bold">
                  {buttonDisabled ? 'Unlocked' : 'Unlock'}
                </Text>
                <Image src={UnlockIcon} ml={2} />
              </>
            )}
          </Flex>
        </Flex>
      )}
    </div>
  );
};
